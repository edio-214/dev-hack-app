class Participant {
  String name;
  bool isChecked;
  int checkID;

  Participant(String name, int checkID) {
    this.name = name;
    this.checkID = checkID;

    if (checkID == 0) {
      this.isChecked = false;
    }
    else {
      this.isChecked = true;
    }
  }
}