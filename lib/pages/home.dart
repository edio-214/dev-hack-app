import 'package:flutter/material.dart';
import 'package:codecheck/services/participant.dart';
import 'package:flutter/rendering.dart';
import 'package:gsheets/gsheets.dart';
import 'package:flutter/services.dart' show rootBundle;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

Future<String> jsonAuth() async {
  String data = await rootBundle.loadString('assets/ucl-check-app-dev-11b0d29fd670.json');
  return data;
}

Future<List<Participant>> getData(String sheetID, String sheetName) async {
  try {
    String auth = await jsonAuth();
    final gsheets = GSheets(auth);
    final participantSheet = await gsheets.spreadsheet(sheetID);
    var sheet = participantSheet.worksheetByTitle(sheetName);

    List<Participant> participants = [];
    final nameColumn = await sheet.cells.column(1);
    final idColumn = await sheet.cells.column(2);

    for(var i = 0; i < nameColumn.length; i++) {
      participants.add(Participant(nameColumn[i].value, int.parse(idColumn[i].value)));
    }
    return participants;
  }
  catch (e) {
    print(e);
  }
}

int defineNextID(List<Participant> participants) {
  bool found = false;
  for (var i = 1; i < participants.length; i++) {
    found = false;
    for (final participant in participants) {
      if (participant.checkID == i) {
        found = true;
        break;
      }
    }
    if (!found) {
      return i;
    }
  }
  return participants.length + 1;
}

int amountCheckedParticipants(List<Participant> participants) {
  int checkedIn = 0;
  for (final participant in participants) {
    if (participant.isChecked) {
      checkedIn++;
    }
  }
  return checkedIn;
}

class _HomeState extends State<Home> {
  Color lightOrange = Color.fromRGBO(255, 123, 45, 1);
  Color darkOrange = Color.fromRGBO(255, 105, 10, 1);

  Future<List> participants;
  List<int> removedIDs = [];

  int nextID = 1;
  int currentlyCheckedParticipants = 0;

  String sheetID = '1MkbF-AgoYUWol-c60UQFWyMjeuCpoLjWAoPLphPFjK4';
  String sheetName = 'teste';

  bool notLoading = true;

  @override
  void initState() {
    super.initState();
    participants = getData(sheetID, sheetName);
  }

  Widget presenceContainer(snapData) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
      child: Card(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
            child: Text(
              'Presença: $currentlyCheckedParticipants / ${snapData.length}',
              style: TextStyle(
                fontSize: 40.0,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget participantCard(int index, snapData) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
      child: Card(
        child: ListTile(
          onTap: () {},
          title: Text(snapData[index].name),
          subtitle: snapData[index].isChecked ? Text('ID: ${snapData[index].checkID}') : Text('Ausente'),
          trailing: notLoading ? Checkbox(
            value: snapData[index].isChecked,
            activeColor: lightOrange,
            onChanged: (bool newValue) async {

              setState(() {
                notLoading = false;
              });

              String auth = await jsonAuth();
              final gsheets = GSheets(auth);
              final participantSheet = await gsheets.spreadsheet(sheetID);
              var sheet = participantSheet.worksheetByTitle(sheetName);

              if(newValue == true) {
                nextID = defineNextID(snapData);
                snapData[index].checkID = nextID;
                await sheet.values.insertValue(nextID, column: 2, row: index+1);
                currentlyCheckedParticipants = amountCheckedParticipants(snapData);
//                participants = getData(sheetID, sheetName); // TODO: nao funciona com future builder, preciso repensar
                _checkinDialog(context, snapData[index].name, snapData[index].checkID);
              }

              if(newValue == false) {
                newValue = await _confirmParticipantRemoval(context, snapData[index].name);
                if(newValue == false) { // participante foi removido, reseta seu ID
//                  removedIDs.add(snapData[index].checkID);
                  setState(() {
                    notLoading = false;
                  });

                  snapData[index].checkID = 0;
                  currentlyCheckedParticipants -= 1;
                  await sheet.values.insertValue(0, column: 2, row: index+1);
                  setState(() {
                    notLoading = true;
                  });
                }
                if(newValue == null) { // caso o usuario aperte o botao de voltar do celular ao inves de usar o popup
                  newValue = true;
                }
              }
              setState(() {
                snapData[index].isChecked = newValue;
                notLoading = true;
              });
            },
          )
          : CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(darkOrange),
          ),
        ),
      ),
    );
  }

  Widget checkInTab(snapData) {
    return ListView.builder(
      itemCount: snapData.length + 1,
      itemBuilder: (context, index) {
        if(index == 0) {
          return presenceContainer(snapData);
        }
        index -=1;
        return participantCard(index, snapData);
      },
    );
  }

  Widget groupTab() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.warning,
            color: darkOrange,
            size: 150.0,
          ),
          Text(
            'Em construção',
            style: TextStyle(
              fontSize: 30.0,
              letterSpacing: 1.5,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.grey[300],
        appBar: AppBar(
          backgroundColor: darkOrange,
          title: Text('Hackfools'),
          centerTitle: true,
          bottom: TabBar(
            indicatorColor: Colors.yellow,
            indicatorWeight: 5.0,
            tabs: <Widget>[
              Tab(text: 'Check-In'),
              Tab(text: 'Grupos'),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            FutureBuilder(
              future: participants,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
//                  nextID = defineNextID(snapshot.data);
                  currentlyCheckedParticipants = amountCheckedParticipants(snapshot.data);
                  return checkInTab(snapshot.data);
                }
                else if (snapshot.hasError) return Center(child: Text("Error"));
                return Center(child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(darkOrange),
                ));
              },
            ),
            groupTab(),
          ],
        ),
      ));
  }
}

Future<void> _checkinDialog(BuildContext context, String name, int id) {
  Color lightOrange = Color.fromRGBO(255, 123, 45, 1);
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Participante Registrado'),
        //content: const Text('This item is no longer available'),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 15.0),
              child: Text(
                '$name',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 15.0),
              child: Text(
                'Participante registrado!',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18.0,
                ),
              ),
            ),
            Text('Seu número de cadastro é'),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 20.0),
              child: Text(
                '$id',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 80.0,
                ),
              ),
            ),
            Text('Informe-o e deseje um bom evento :)'),
          ],
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(
              'OK',
              style: TextStyle(
                color: lightOrange,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Future<bool> _confirmParticipantRemoval(BuildContext context, String name) async {
  Color lightOrange = Color.fromRGBO(255, 123, 45, 1);
  return showDialog<bool>(
    context: context,
    barrierDismissible: false, // user must tap button for close dialog!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Remover a presença do participante?'),
        content: Text('Tem certeza que deseja remover a presença do participante $name?'),
        actions: <Widget>[
          FlatButton(
            child: Text(
              'CANCELAR',
              style: TextStyle(
                color: lightOrange,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop(true);
            },
          ),
          FlatButton(
            child: Text(
              'REMOVER',
              style: TextStyle(
                color: lightOrange,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          )
        ],
      );
    },
  );
}