import 'package:flutter/material.dart';
import 'dart:async';

class Loading extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    Timer(Duration(milliseconds: 500), () {
      Navigator.pushReplacementNamed(context, '/home');
    });

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            //colors: [Color.fromRGBO(0xff8b46), Color(0xff690a)],
            colors: [Color.fromRGBO(255, 123, 45, 1), Color.fromRGBO(255, 105, 10, 1)],
          ),
        ),
        child: Center(
          child: CircleAvatar(
            backgroundImage: AssetImage('assets/ucl-logo.png'),
            radius: 100.0,
          ),
        ),
      ),
    );
  }
}
