import 'package:flutter/material.dart';
import 'package:codecheck/pages/loading.dart';
import 'package:codecheck/pages/home.dart';

void main() => runApp(MaterialApp(
  initialRoute: '/',
  routes: {
    '/': (context) => Loading(),
    '/home': (context) => Home(),
  },
)); // MaterialApp